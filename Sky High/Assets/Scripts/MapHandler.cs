﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapHandler : MonoBehaviour
{
    [SerializeField] private List<GameObject> roomPrefabs;                      //the room prefabs
    Floor currentFloor;

    // Start is called before the first frame update
    void Start()
    {
        Initilize();
    }

    // Update is called once per frame
    void Update()
    {
        //nothing here yet
    }

    void Initilize()
    {
        //create nessecary objects and map variables
        currentFloor = new Floor();
        currentFloor.Initilize(5,5,6, roomPrefabs);
    }


}
